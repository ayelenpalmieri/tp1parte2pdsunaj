﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiRestFull.AccessData.Migrations
{
    public partial class ApiRestFull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Cliente",
                schema: "dbo",
                columns: table => new
                {
                    ClienteId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DNI = table.Column<string>(type: "NVARCHAR(10)", maxLength: 10, nullable: false),
                    Nombre = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Apellido = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Direccion = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Telefono = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cliente", x => x.ClienteId);
                });

            migrationBuilder.CreateTable(
                name: "Producto",
                schema: "dbo",
                columns: table => new
                {
                    ProductoId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Codigo = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Marca = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Nombre = table.Column<string>(type: "NVARCHAR(45)", maxLength: 45, nullable: false),
                    Precio = table.Column<decimal>(type: "decimal(15, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Producto", x => x.ProductoId);
                });

            migrationBuilder.CreateTable(
                name: "Carrito",
                schema: "dbo",
                columns: table => new
                {
                    CarritoId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClienteId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carrito", x => x.CarritoId);
                    table.ForeignKey(
                        name: "FK_Carrito_Cliente_ClienteId",
                        column: x => x.ClienteId,
                        principalSchema: "dbo",
                        principalTable: "Cliente",
                        principalColumn: "ClienteId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Carrito_Producto",
                schema: "dbo",
                columns: table => new
                {
                    Carrito_ProductoId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CarritoId = table.Column<int>(type: "INTEGER", nullable: false),
                    ProductoId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Carrito_Producto", x => x.Carrito_ProductoId);
                    table.ForeignKey(
                        name: "FK_Carrito_Producto_Carrito_CarritoId",
                        column: x => x.CarritoId,
                        principalSchema: "dbo",
                        principalTable: "Carrito",
                        principalColumn: "CarritoId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Carrito_Producto_Producto_ProductoId",
                        column: x => x.ProductoId,
                        principalSchema: "dbo",
                        principalTable: "Producto",
                        principalColumn: "ProductoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Ventas",
                schema: "dbo",
                columns: table => new
                {
                    VentasId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CarritoId = table.Column<int>(type: "INTEGER", nullable: false),
                    Fecha = table.Column<DateTime>(type: "DATETIME", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ventas", x => x.VentasId);
                    table.ForeignKey(
                        name: "FK_Ventas_Carrito_CarritoId",
                        column: x => x.CarritoId,
                        principalSchema: "dbo",
                        principalTable: "Carrito",
                        principalColumn: "CarritoId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Cliente",
                columns: new[] { "ClienteId", "Apellido", "DNI", "Direccion", "Nombre", "Telefono" },
                values: new object[,]
                {
                    { 1, "Palmieri", "35095481", "Av Libres 456", "Ayelen", "1564894562" },
                    { 2, "Lopez", "33025685", "Av Melvin 123", "Florencia", "1546784578" },
                    { 3, "Sanchez", "35485784", "Av San Martin 256", "Dario", "1546521245" }
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Producto",
                columns: new[] { "ProductoId", "Codigo", "Marca", "Nombre", "Precio" },
                values: new object[,]
                {
                    { 14, "PS-X466", "Philips", "Monitor - Philips 24'' Led FullHd 1080p 60Hz", 25999.01m },
                    { 13, "MG279Q", "Asus", "Monitor - Asus MG279 27'' lps 144Hz Freesync", 98799.99m },
                    { 12, "U32J59OUQ", "Samsung", "Monitor - Samsung WQHD 32'' Curved Led", 79995.01m },
                    { 11, "HP-250G7", "HP", "Notebook - HP 250 G7 Intel Core i3 8gb Ssd", 53999.99m },
                    { 10, "I3593-3992BLK", "Dell", "Notebook - Dell Intel i3 8gb 1tb 128gb Ssd", 99999.99m },
                    { 9, "SX12-8565U", "Sony Vaio", "Notebook - Vaio SX12 Intel Core I7 16gb 1tb", 336279.99m },
                    { 8, "XG32VQ", "Asus", "Monitor - Asus ROG Strix 32'' 2K 144Hz", 38907.81m },
                    { 7, "G531", "Asus", "Notebook - Asus ROG Strix I7 8gb Gtx1650 Ssd ", 218990.01m },
                    { 6, "SM-A105MZ", "Samsung", "SmartPhone - Samsung Galaxy A10 32gb 2gb RAM", 31999.99m },
                    { 5, "SM-S205XA", "Samsung", "SmartPhone - Samsung Galaxy S20 128gb 8gb RAM", 89900.99m },
                    { 4, "LMX520HM", "LG", "SmartPhone - LG Serie K50 32gb 3gb RAM DUAL P", 31998.99m },
                    { 3, "S5PXW-I672", "SkullCandy", "Audicular - SkullCandy Riff Wireless Black", 11399.99m },
                    { 2, "WH1000XM3", "Sony", "Audicular - Sony Wireless Stereo Carg Rapida", 18999.01m },
                    { 1, "M-MOV128", "Motorola", "SmartPhone - Moto One Vision 128gb 4gb RAM", 45000.99m },
                    { 15, "PS-X466", "Panasonic", "Audicular - Panasonic Rp Full-sized Liviano", 9399.99m },
                    { 16, "MT-SH012", "Motorola", "Audicular - Motorola Pulse Escape Wireless", 4500.01m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Carrito_ClienteId",
                schema: "dbo",
                table: "Carrito",
                column: "ClienteId");

            migrationBuilder.CreateIndex(
                name: "IX_Carrito_Producto_CarritoId",
                schema: "dbo",
                table: "Carrito_Producto",
                column: "CarritoId");

            migrationBuilder.CreateIndex(
                name: "IX_Carrito_Producto_ProductoId",
                schema: "dbo",
                table: "Carrito_Producto",
                column: "ProductoId");

            migrationBuilder.CreateIndex(
                name: "IX_Ventas_CarritoId",
                schema: "dbo",
                table: "Ventas",
                column: "CarritoId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Carrito_Producto",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Ventas",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Producto",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Carrito",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Cliente",
                schema: "dbo");
        }
    }
}
