﻿using ApiRestFull.Domain.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.AccessData.Commands
{
    public class GenericsRepository : IGenericsRepository
    {
        private readonly ApiRestFullDbContext _context;

        public GenericsRepository(ApiRestFullDbContext context)
        {
            this._context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
            _context.SaveChanges();
        }

    }
}
