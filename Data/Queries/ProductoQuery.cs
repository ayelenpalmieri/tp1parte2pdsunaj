﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Queries;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ApiRestFull.AccessData.Queries
{
    public class ProductoQuery : IProductoQuery
    {
        private readonly IDbConnection _connection;
        private readonly Compiler _sqlKataCompiler;

        public ProductoQuery(IDbConnection connection, Compiler sqlKataCompiler)
        {
            this._connection = connection;
            this._sqlKataCompiler = sqlKataCompiler;
        }

        public List<ProductoDto> GetAllProductos()
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var query = db.Query("Producto"); //select all

            var result = query.Get<ProductoDto>();

            return result.ToList();
        }

        public ProductoDto GetProductobyID(int productoId)
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var producto = db.Query("Producto")
                .Select("Producto.Codigo", "Producto.Marca", "Producto.Nombre", "Producto.Precio")
                .Where("ProductoId", "=", productoId)
                .FirstOrDefault<ProductoDto>();

            if (producto != null)
            {
                return new ProductoDto
                {
                    Codigo = producto.Codigo,
                    Marca = producto.Marca,
                    Nombre = producto.Nombre,
                    Precio = producto.Precio
                };
            }
            else
            {
                return null;
            }         
        }


        public ProductoDto GetProductobyCodigo(string codigo)
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var producto = db.Query("Producto")
                .Select("Producto.Codigo", "Producto.Marca", "Producto.Nombre", "Producto.Precio")
                .Where("Codigo", "=", codigo)
                .FirstOrDefault<ProductoDto>();


            return new ProductoDto
            {
                Codigo = producto.Codigo,
                Marca = producto.Marca,
                Nombre = producto.Nombre,
                Precio = producto.Precio
            };
        }
    }
}
