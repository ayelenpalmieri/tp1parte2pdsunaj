﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Queries;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace ApiRestFull.AccessData.Queries
{
    public class ClienteQuery: IClienteQuery
    {
        private readonly IDbConnection _connection;
        private readonly Compiler _sqlKataCompiler;

        public ClienteQuery(IDbConnection connection, Compiler sqlKataCompiler)
        {
            this._connection = connection;
            this._sqlKataCompiler = sqlKataCompiler;
        }

        public List<ClienteDto> GetAllClientes()
        {
            var db = new QueryFactory(_connection,_sqlKataCompiler);

            var query = db.Query("Cliente"); //select all

            var result = query.Get<ClienteDto>();

            return result.ToList();
        }

        public ClienteDto GetClientebyDNI(string DNI)
        {
            try
            {
                var db = new QueryFactory(_connection, _sqlKataCompiler);

                var cliente = db.Query("Cliente")
                    .Select("Cliente.ClienteId", "Cliente.DNI", "Cliente.Nombre", "Cliente.Apellido", "Cliente.Direccion", "Cliente.Telefono")
                    .Where("DNI", "=", DNI)
                    .FirstOrDefault<ClienteDto>();

                if (cliente == null)
                {
                    return new ClienteDto();
                }

                return new ClienteDto
                {
                    ClienteId = cliente.ClienteId,
                    DNI = cliente.DNI,
                    Nombre = cliente.Nombre,
                    Apellido = cliente.Apellido,
                    Direccion = cliente.Direccion,
                    Telefono = cliente.Telefono
                };
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
