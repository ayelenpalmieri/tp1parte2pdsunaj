﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion.Internal;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ApiRestFull.AccessData.Queries
{
    public class VentasQuery : IVentasQuery
    {
        private readonly IDbConnection _connection;
        private readonly Compiler _sqlKataCompiler;

        public VentasQuery(IDbConnection connection, Compiler sqlKataCompiler)
        {
            this._connection = connection;
            this._sqlKataCompiler = sqlKataCompiler;
        }

        public VentasDto GetVentaDtoByIdCarrito(int CarritoId)
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var venta = db.Query("Ventas")
                .Select("Ventas.CarritoId", "Ventas.Fecha")
                .Where("CarritoId", "=", CarritoId)
                .FirstOrDefault<VentasDto>();

            return new VentasDto
            {
                CarritoId = venta.CarritoId,
                Fecha = venta.Fecha,
            };
        }

        public List<ResponseGetAllVentasDto> GetVentas(string fecha)
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var listadoVentas = db.Query("Ventas")
                .Select("Cliente.DNI", "Cliente.Apellido", "Ventas.Fecha", "Producto.Codigo", "Producto.Marca", "Producto.Nombre", "Producto.Precio")
                .When(!string.IsNullOrWhiteSpace(fecha), q => q.WhereDate("Ventas.Fecha", $"{fecha}"))
                .Join("Carrito", "Carrito.CarritoId", "Ventas.CarritoId")
                .Join("Cliente", "Cliente.ClienteId", "Carrito.ClienteId")
                .Join("Carrito_Producto", "Carrito_Producto.CarritoId", "Carrito.CarritoId")
                .Join("Producto", "Producto.ProductoId", "Carrito_Producto.ProductoId");

            var listado = listadoVentas.Get<ResponseGetAllVentasDto>();
            return listado.ToList();
        }

    }
}
