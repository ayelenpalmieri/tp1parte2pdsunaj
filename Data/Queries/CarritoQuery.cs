﻿using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using SqlKata.Compilers;
using SqlKata.Execution;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace ApiRestFull.AccessData.Queries
{
    public class CarritoQuery : ICarritoQuery
    {
        private readonly IDbConnection _connection;
        private readonly Compiler _sqlKataCompiler;

        public CarritoQuery(IDbConnection connection, Compiler sqlKataCompiler)
        {
            this._connection = connection;
            this._sqlKataCompiler = sqlKataCompiler;
        }

        public CarritoDto GetCarritoDtoByIdCliente(int ClienteId)
        {
            var db = new QueryFactory(_connection, _sqlKataCompiler);

            var carrito = db.Query("Carrito")
                .Select("Carrito.CarritoId", "Carrito.ClienteId")
                .Where("ClienteId", "=", ClienteId)
                .FirstOrDefault<CarritoDto>();

            return new CarritoDto
            {
                CarritoId = carrito.CarritoId,
                ClienteId = carrito.ClienteId,
            };


        }
    }
}
