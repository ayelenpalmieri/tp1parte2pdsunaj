﻿using ApiRestFull.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace ApiRestFull.AccessData
{
    public class ApiRestFullDbContext : DbContext
    {
        public ApiRestFullDbContext()
        {
        }

        public ApiRestFullDbContext(DbContextOptions<ApiRestFullDbContext> options) : base(options)

        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Cliente>().Property(t =>
                t.ClienteId).IsRequired().HasColumnType("INTEGER").HasColumnName("ClienteId");
            modelBuilder.Entity<Cliente>().HasKey(t => t.ClienteId);
            modelBuilder.Entity<Cliente>().ToTable("Cliente", "dbo");

            modelBuilder.Entity<Cliente>().Property(t => 
                t.DNI).IsRequired().HasColumnName("DNI").HasColumnType("NVARCHAR(10)").HasMaxLength(10);

            modelBuilder.Entity<Cliente>().Property(t => 
                t.Nombre).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Nombre").HasMaxLength(45);

            modelBuilder.Entity<Cliente>().Property(t => 
                t.Apellido).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Apellido").HasMaxLength(45);

            modelBuilder.Entity<Cliente>().Property(t => 
                t.Direccion).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Direccion").HasMaxLength(45);

            modelBuilder.Entity<Cliente>().Property(t => 
                t.Telefono).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Telefono").HasMaxLength(45);
            

            modelBuilder.Entity<Carrito>().Property(t => 
                t.CarritoId).IsRequired().HasColumnType("INTEGER").HasColumnName("CarritoId");
            modelBuilder.Entity<Carrito>().HasKey(t => t.CarritoId);
            modelBuilder.Entity<Carrito>().ToTable("Carrito", "dbo");

            modelBuilder.Entity<Carrito>().Property(t => 
                t.ClienteId).IsRequired().HasColumnType("INTEGER").HasColumnName("ClienteId");

            modelBuilder.Entity<Ventas>().Property(t => 
                t.VentasId).IsRequired().HasColumnType("INTEGER").HasColumnName("VentasId");
            modelBuilder.Entity<Ventas>().HasKey(t => t.VentasId);
            modelBuilder.Entity<Ventas>().ToTable("Ventas", "dbo");

            modelBuilder.Entity<Ventas>().Property(t =>
                t.CarritoId).IsRequired().HasColumnType("INTEGER").HasColumnName("CarritoId");

            modelBuilder.Entity<Ventas>().Property(t =>
                t.Fecha).IsRequired().HasColumnType("DATETIME").HasColumnName("Fecha");

            modelBuilder.Entity<Carrito_Producto>().Property(t =>
                t.Carrito_ProductoId).IsRequired().HasColumnType("INTEGER").HasColumnName("Carrito_ProductoId");
            modelBuilder.Entity<Carrito_Producto>().HasKey(t => t.Carrito_ProductoId);
            modelBuilder.Entity<Carrito_Producto>().ToTable("Carrito_Producto", "dbo");

            modelBuilder.Entity<Carrito_Producto>().Property(t =>
                t.CarritoId).IsRequired().HasColumnType("INTEGER").HasColumnName("CarritoId");

            modelBuilder.Entity<Carrito_Producto>().Property(t =>
                t.ProductoId).IsRequired().HasColumnType("INTEGER").HasColumnName("ProductoId");
         
            modelBuilder.Entity<Producto>().Property(t => 
                t.ProductoId).IsRequired().HasColumnType("INTEGER").HasColumnName("ProductoId");
            modelBuilder.Entity<Producto>().HasKey(t => t.ProductoId);
            modelBuilder.Entity<Producto>().ToTable("Producto", "dbo");

            modelBuilder.Entity<Producto>().Property(t => 
                t.Codigo).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Codigo").HasMaxLength(45);

            modelBuilder.Entity<Producto>().Property(t => 
                t.Marca).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Marca").HasMaxLength(45);

            modelBuilder.Entity<Producto>().Property(t => 
                t.Nombre).IsRequired().HasColumnType("NVARCHAR(45)").HasColumnName("Nombre").HasMaxLength(45);

            modelBuilder.Entity<Producto>().Property(t =>
                t.Precio).IsRequired().HasColumnType("DECIMAL").HasColumnName("Precio");

            //modelBuilder.Entity<Producto>().Property(t => t.Precio).HasPrecision(12, 10);

            foreach (var property in modelBuilder.Model.GetEntityTypes()
                           .SelectMany(t => t.GetProperties())
                          .Where(p => p.ClrType == typeof(Decimal)))
            {
                property.SetColumnType("decimal(15, 2)");
            }

            modelBuilder.Entity<Cliente>().HasData(
                        new Cliente
                        {
                            ClienteId = 1,
                            DNI = "35095481",
                            Nombre = "Ayelen",
                            Apellido = "Palmieri",
                            Direccion = "Av Libres 456",
                            Telefono = "1564894562",
                        },
                        new Cliente
                        {
                            ClienteId = 2,
                            DNI = "33025685",
                            Nombre = "Florencia",
                            Apellido = "Lopez",
                            Direccion = "Av Melvin 123",
                            Telefono = "1546784578",
                        }, 
                        new Cliente
                        {
                            ClienteId = 3,
                            DNI = "35485784",
                            Nombre = "Dario",
                            Apellido = "Sanchez",
                            Direccion = "Av San Martin 256",
                            Telefono = "1546521245",
                        }
            );

            modelBuilder.Entity<Producto>().HasData(
                        new Producto
                        {
                            ProductoId = 1,
                            Codigo = "M-MOV128",
                            Marca = "Motorola",
                            Nombre = "SmartPhone - Moto One Vision 128gb 4gb RAM",
                            Precio = 45000.99m,
                        },
                        new Producto
                        {
                            ProductoId = 2,
                            Codigo = "WH1000XM3",
                            Marca = "Sony",
                            Nombre = "Audicular - Sony Wireless Stereo Carg Rapida",
                            Precio = 18999.01m,
                        },
                        new Producto
                        {
                            ProductoId = 3,
                            Codigo = "S5PXW-I672",
                            Marca = "SkullCandy",
                            Nombre = "Audicular - SkullCandy Riff Wireless Black",
                            Precio = 11399.99m,
                        },
                        new Producto
                        {
                            ProductoId = 4,
                            Codigo = "LMX520HM",
                            Marca = "LG",
                            Nombre = "SmartPhone - LG Serie K50 32gb 3gb RAM DUAL P",
                            Precio = 31998.99m,
                        },
                        new Producto
                        {
                            ProductoId = 5,
                            Codigo = "SM-S205XA",
                            Marca = "Samsung",
                            Nombre = "SmartPhone - Samsung Galaxy S20 128gb 8gb RAM",
                            Precio = 89900.99m,
                        },
                        new Producto
                        {
                            ProductoId = 6,
                            Codigo = "SM-A105MZ",
                            Marca = "Samsung",
                            Nombre = "SmartPhone - Samsung Galaxy A10 32gb 2gb RAM",
                            Precio = 31999.99m,
                        },
                        new Producto
                        {
                            ProductoId = 7,
                            Codigo = "G531",
                            Marca = "Asus",
                            Nombre = "Notebook - Asus ROG Strix I7 8gb Gtx1650 Ssd ",
                            Precio = 218990.01m,
                        },
                        new Producto
                        {
                            ProductoId = 8,
                            Codigo = "XG32VQ",
                            Marca = "Asus",
                            Nombre = "Monitor - Asus ROG Strix 32'' 2K 144Hz",
                            Precio = 38907.81m,
                        },
                        new Producto
                        {
                            ProductoId = 9,
                            Codigo = "SX12-8565U",
                            Marca = "Sony Vaio",
                            Nombre = "Notebook - Vaio SX12 Intel Core I7 16gb 1tb",
                            Precio = 336279.99m,
                        },
                        new Producto
                        {
                            ProductoId = 10,
                            Codigo = "I3593-3992BLK",
                            Marca = "Dell",
                            Nombre = "Notebook - Dell Intel i3 8gb 1tb 128gb Ssd",
                            Precio = 99999.99m,
                        },
                        new Producto
                        {
                            ProductoId = 11,
                            Codigo = "HP-250G7",
                            Marca = "HP",
                            Nombre = "Notebook - HP 250 G7 Intel Core i3 8gb Ssd",
                            Precio = 53999.99m,
                        },
                        new Producto
                        {
                            ProductoId = 12,
                            Codigo = "U32J59OUQ",
                            Marca = "Samsung",
                            Nombre = "Monitor - Samsung WQHD 32'' Curved Led",
                            Precio = 79995.01m,
                        },
                        new Producto
                        {
                            ProductoId = 13,
                            Codigo = "MG279Q",
                            Marca = "Asus",
                            Nombre = "Monitor - Asus MG279 27'' lps 144Hz Freesync",
                            Precio = 98799.99m,
                        },
                        new Producto
                        {
                            ProductoId = 14,
                            Codigo = "PS-X466",
                            Marca = "Philips",
                            Nombre = "Monitor - Philips 24'' Led FullHd 1080p 60Hz",
                            Precio = 25999.01m,
                        },
                        new Producto
                        {
                            ProductoId = 15,
                            Codigo = "PS-X466",
                            Marca = "Panasonic",
                            Nombre = "Audicular - Panasonic Rp Full-sized Liviano",
                            Precio = 9399.99m,
                        },
                        new Producto
                        {
                            ProductoId = 16,
                            Codigo = "MT-SH012",
                            Marca = "Motorola",
                            Nombre = "Audicular - Motorola Pulse Escape Wireless",
                            Precio = 4500.01m,
                        }
                );

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Carrito> Carritos { get; set; }
        public DbSet<Ventas> Ventas { get; set; }
        public DbSet<Carrito_Producto> Carrito_Productos { get; set; }
        public DbSet<Producto> Productos { get; set; }

    }
}
