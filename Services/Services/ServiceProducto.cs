﻿using ApiRestFull.Domain.Commands;
using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public class ServiceProducto : IServiceProducto
    {
        private readonly IGenericsRepository _repository;
        private readonly IProductoQuery _query;

        public ServiceProducto(IGenericsRepository repository, IProductoQuery query)
        {
            this._repository = repository;
            this._query = query;
        }
        public ProductoDto GetProductobyCodigo(string Codigo)
        {
            return _query.GetProductobyCodigo(Codigo);
        }

        public ProductoDto GetProductobyID(int ProductoId)
        {
            return _query.GetProductobyID(ProductoId);
        }

        public bool ProductExists(int ProductoId)
        {
            ProductoDto producto = _query.GetProductobyID(ProductoId);
            return true;
        }

        public IEnumerable<ProductoDto> GetProductos()
        {
            return _query.GetAllProductos();
        }

        public ProductoDto SetProducto(ProductoDto producto)
        {
            var entity = new Producto
            {
                Codigo = producto.Codigo,
                Marca = producto.Marca,
                Nombre = producto.Nombre,
                Precio = producto.Precio,
            };

            _repository.Add<Producto>(entity);
            return producto;
        }

    }
}
