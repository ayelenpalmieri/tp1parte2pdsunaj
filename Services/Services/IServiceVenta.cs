﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public interface IServiceVenta
    {
        VentasDto SetVenta(CarritoDto carrito);
        VentasDto GetVentaDtoByIdCarrito(int CarritoId);
        List<ResponseGetAllVentasDto> GetVentas(string fecha);
        List<ResponseGetAllVentasDto> GetProductosinLista(string Codigo, string fecha);


    }
}
