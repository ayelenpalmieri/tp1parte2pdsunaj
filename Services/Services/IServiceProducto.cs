﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public interface IServiceProducto
    {
        ProductoDto SetProducto(ProductoDto producto);
        IEnumerable<ProductoDto> GetProductos();
        ProductoDto GetProductobyCodigo(string Codigo);
        ProductoDto GetProductobyID(int ProductoId);
    }
}
