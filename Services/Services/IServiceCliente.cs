﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public interface IServiceCliente
    {
        ClienteDto SetCliente(ClienteDto cliente);
        List<ClienteDto> GetClientes();
        ClienteDto GetClientebyDNI(string DNI);
    }
}
