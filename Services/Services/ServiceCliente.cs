﻿using ApiRestFull.Domain.Commands;
using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public class ServiceCliente : IServiceCliente
    {
        private readonly IGenericsRepository _repository;
        private readonly IClienteQuery _query;
        public ServiceCliente(IGenericsRepository repository, IClienteQuery query)
        {
            this._repository = repository;
            this._query = query;
        }

        public ClienteDto GetClientebyDNI(string DNI)
        {
            return _query.GetClientebyDNI(DNI);
        }


        public List<ClienteDto> GetClientes()
        {
            return _query.GetAllClientes();
        }

        public ClienteDto SetCliente(ClienteDto cliente)
        {
            var entity = new Cliente
            {
                DNI = cliente.DNI,
                Nombre = cliente.Nombre,
                Apellido = cliente.Apellido,
                Direccion = cliente.Direccion,
                Telefono = cliente.Telefono,
            };

            _repository.Add<Cliente>(entity);
            return cliente;
        }

    }
}
