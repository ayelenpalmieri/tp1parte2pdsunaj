﻿using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public interface IServiceCarrito
    {
        CarritoDto SetCarrito(Cliente cliente);
        Carrito GetCarrito(int CarritoId);
        CarritoDto GetCarritoDtoByIdCliente(int ClienteId);
        void SetCarritoProducto(CarritoDto carrito, Producto producto);
    }
}
