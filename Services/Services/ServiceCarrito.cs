﻿using ApiRestFull.Domain.Commands;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public class ServiceCarrito : IServiceCarrito
    {
        private readonly IGenericsRepository _repository;
        private readonly ICarritoQuery _query;

        public ServiceCarrito(IGenericsRepository repository, ICarritoQuery query)
        {
            this._repository = repository;
            this._query = query;
        }

        public Carrito GetCarrito(int CarritoId)
        {
            throw new NotImplementedException();
            //Carrito carrito = this.GetVentas().FirstOrDefault(v => v.VentasId == VentaId);
            //return venta;
        }

        public CarritoDto SetCarrito(Cliente cliente)
        {
            var clienteId = cliente.ClienteId;

            var entity = new Carrito
            {
                ClienteId = clienteId,
            };

            _repository.Add<Carrito>(entity);
            return this.GetCarritoDtoByIdCliente(clienteId);
        }

        public CarritoDto GetCarritoDtoByIdCliente(int ClienteId)
        {

            return _query.GetCarritoDtoByIdCliente(ClienteId);
        }

        public void SetCarritoProducto(CarritoDto carrito, Producto producto)
        {
            var carritoId = carrito.CarritoId;
            var productoId = producto.ProductoId;

            try
            {
                var entity = new Carrito_Producto
                {
                    CarritoId = carritoId,
                    ProductoId = productoId
                };

                _repository.Add<Carrito_Producto>(entity);

            }
            catch (Exception e)
            { 
                
            }

        }
    }
}


