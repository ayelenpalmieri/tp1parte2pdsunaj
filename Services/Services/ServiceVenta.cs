﻿using ApiRestFull.Domain.Commands;
using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using ApiRestFull.Domain.Queries;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ApiRestFull.Application.Services
{
    public class ServiceVenta : IServiceVenta
    {
        private readonly IGenericsRepository _repository;
        private readonly IVentasQuery _query;

        public ServiceVenta(IGenericsRepository repository, IVentasQuery query)
        {
            this._repository = repository;
            this._query = query;
        }


        public VentasDto SetVenta(CarritoDto carrito)
        {
            try
            {
                var carritoId = carrito.CarritoId;

                var entity = new Ventas
                {
                    CarritoId = carritoId,
                    Fecha = DateTime.Now
                };

                _repository.Add<Ventas>(entity);
                return this.GetVentaDtoByIdCarrito(carritoId);
            }
            catch (Exception e) {
                throw e;
            }
        }


        public VentasDto GetVentaDtoByIdCarrito(int CarritoId)
        {

            return _query.GetVentaDtoByIdCarrito(CarritoId);
        }


        public List<ResponseGetAllVentasDto> GetVentas(string fecha)
        {
            return _query.GetVentas(fecha);
        }

        public List<ResponseGetAllVentasDto> GetProductosinLista(string Codigo, string fecha)
        {
            var listaVentas = GetVentas(fecha);
            List<ResponseGetAllVentasDto> nuevaListaVentas = new List<ResponseGetAllVentasDto>();
            foreach (ResponseGetAllVentasDto producto in listaVentas)
            {
                if (producto.Codigo == Codigo)
                {
                    nuevaListaVentas.Add(producto);
                }
            }return nuevaListaVentas;
        }
    }
}
