﻿using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Queries
{
    public interface IVentasQuery
    {
        List<ResponseGetAllVentasDto> GetVentas(string fecha);
        VentasDto GetVentaDtoByIdCarrito(int CarritoId);
    }
}
