﻿using ApiRestFull.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Queries
{
    public interface ICarritoQuery
    {
        CarritoDto GetCarritoDtoByIdCliente(int ClienteId);
    }
}
