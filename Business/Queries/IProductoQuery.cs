﻿using ApiRestFull.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Queries
{
    public interface IProductoQuery
    {
        List<ProductoDto> GetAllProductos();
        ProductoDto GetProductobyCodigo(string codigo);
        ProductoDto GetProductobyID(int productoId);
    }
}
