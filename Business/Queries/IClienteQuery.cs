﻿using ApiRestFull.Domain.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Queries
{
    public interface IClienteQuery 
    {
        List<ClienteDto> GetAllClientes();

        ClienteDto GetClientebyDNI(string DNI);

    }
}
