﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.DTOs
{
    public class ClienteDto
    {
        public int ClienteId { get; set; }
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

    }
}
