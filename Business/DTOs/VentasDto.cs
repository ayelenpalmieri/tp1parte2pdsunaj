﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Models
{
    public class VentasDto
    {
        public int CarritoId { get; set; }
        public DateTime Fecha { get; set; }

    }
}
