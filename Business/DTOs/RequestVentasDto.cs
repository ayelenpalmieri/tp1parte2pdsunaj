﻿using ApiRestFull.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.DTOs
{
    public class RequestVentasDto
    {
        public Cliente cliente { get; set; }
        public List<Producto> productos { get; set; }

    }
}
