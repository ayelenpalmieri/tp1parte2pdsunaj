﻿using ApiRestFull.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.DTOs
{
    public class ResponseGetAllVentasDto
    {
        public string DNI { get; set; }
        public string Apellido { get; set; }
        public DateTime Fecha { get; set; }
        public string Codigo { get; set; }
        public string Marca { get; set; }
        public string Nombre { get; set; }
        public Decimal Precio { get; set; }

    }
}
