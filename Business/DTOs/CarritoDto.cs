﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Models
{
    public class CarritoDto
    {
        public int CarritoId { get; set; }
        public int ClienteId { get; set; }
    }
}
