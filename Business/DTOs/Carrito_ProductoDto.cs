﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Models
{
    public class Carrito_ProductoDto
    {
        public int CarritoId { get; set; }
        public int ProductoId { get; set; }
    }
}
