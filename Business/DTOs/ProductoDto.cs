﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.DTOs
{
    public class ProductoDto
    {
        public int ProductoId { get; set; }
        public string Codigo { get; set; }
        public string Marca { get; set; }
        public string Nombre { get; set; }
        public Decimal Precio { get; set; }
    }
}
