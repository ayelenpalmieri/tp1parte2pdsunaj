﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Entities
{
    public class Carrito
    {
        public Carrito()
        {
        }

        public int CarritoId { get; set; }
        public int ClienteId { get; set; }

        public Cliente Cliente { get; set; }

    }
}
