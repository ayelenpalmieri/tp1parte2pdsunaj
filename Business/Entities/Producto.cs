﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Entities
{
    public class Producto
    {
        public Producto()
        {
            Carrito_Producto = new HashSet<Carrito_Producto>();
        }

        public int ProductoId { get; set; }
        public string Codigo { get; set; }
        public string Marca { get; set; }
        public string Nombre { get; set; }
        public Decimal Precio { get; set; }

        public ICollection<Carrito_Producto> Carrito_Producto { get; set; }
    }
}
