﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Entities
{
    public class Cliente
    {
        public Cliente()
        {
            Carrito = new HashSet<Carrito>();
        }

        public int ClienteId { get; set; }
        public string DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }

        public ICollection<Carrito> Carrito { get; set; }

    }
}
