﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Entities
{
    public class Carrito_Producto
    {
        public Carrito_Producto()
        {
        }

        public int Carrito_ProductoId { get; set; }
        public int CarritoId { get; set; }
        public int ProductoId { get; set; }

        public Producto Producto { get; set; }
        public Carrito Carrito { get; set; }

    }
}
