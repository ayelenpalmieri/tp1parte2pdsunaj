﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiRestFull.Domain.Entities
{
    public class Ventas
    {
        public Ventas()
        { 
        }

        public int VentasId { get; set; }
        public int CarritoId { get; set; }
        public DateTime Fecha { get; set; }

        public Carrito Carrito { get; set; }
    }
}
