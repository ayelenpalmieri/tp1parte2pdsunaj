1. Debe permitir registrar las ventas del dia

[POST /api/ventas]
[HttpPost] ----->Json

https://localhost:44344/api/ventas
[FromBody] RequestVentasDto

{
	"Cliente":
	{
		"ClienteId": 4,
		"DNI": "35095481",
		"Nombre": "Ayelen",
		"Apellido": "Palmieri",
		"Direccion": "Pringles 456",
		"Telefono": "1564894562"
	},
	
	"Productos": [

    {
      "ProductoId": 4,
      "Codigo": "ZAQ-897",
      "Marca": "LG",
      "Nombre": "LG Serie K",
      "Precio": 17555.80
    },
       {
      "ProductoId": 5,
      "Codigo": "MOP-567",
      "Marca": "Samsung",
      "Nombre": "Samsung S10",
      "Precio": 24999.90
    }
  ]
}

---------------------------------------------------------------------------------
2. Debe realizar reportes de la ventas del dia (Listado de ventas).

//[GET /api/ventas]
[HttpGet("{fecha}")] ----->string

https://localhost:44344/api/ventas/2020-05-13

---------------------------------------------------------------------------------
3. Debe permitir realizar búsquedas por producto en el listado de ventas.

//[GET /api/ventas]
[HttpGet("fecha/{fecha}/codigo/{codigo}")] ----->string

https://localhost:44344/api/ventas/fecha/2020-05-13/codigo/ZAQ-897
   
---------------------------------------------------------------------------------
4. Debe permitir registrar a los clientes y los productos.

//[POST /api/cliente]
[HttpPost] ----->Json
 
https://localhost:44344/api/cliente
[FromBody] ClienteDto 

{
	"DNI" : "35021258",
	"Nombre" : "Florencia",
	"Apellido" : "Perez",
	"Telefono" : "1564912458"
}

//[POST /api/producto]
[HttpPost]

https://localhost:44344/api/producto
[FromBody] ProductoDto 

{
    "Codigo": "ASD-123",
    "Marca": "Samsung",
    "Nombre": "Samsung Galaxy A10",
    "Precio": 19999.99
}

---------------------------------------------------------------------------------
5. Un cliente puede cargar varios productos en su compra

[POST /api/ventas]
[HttpPost] ----->Json

https://localhost:44344/api/ventas
[FromBody] RequestVentasDto

{
	"Cliente":
	{
		"ClienteId": 4,
		"DNI": "35095481",
		"Nombre": "Ayelen",
		"Apellido": "Palmieri",
		"Direccion": "Pringles 456",
		"Telefono": "1564894562"
	},
	
	"Productos": [

    {
      "ProductoId": 4,
      "Codigo": "ZAQ-897",
      "Marca": "LG",
      "Nombre": "LG Serie K",
      "Precio": 17555.80
    },
       {
      "ProductoId": 5,
      "Codigo": "MOP-567",
      "Marca": "Samsung",
      "Nombre": "Samsung S10",
      "Precio": 24999.90
    }
  ]
}

---------------------------------------------------------------------------------
6. Debe devolver la lista de clientes y productos.

//[GET /api/cliente]
[HttpGet]

https://localhost:44344/api/cliente

//[GET /api/producto]
[HttpGet]

https://localhost:44344/api/producto

---------------------------------------------------------------------------------
7. Agregar busqueda de clientes por DNI o Nombre y apellido.

//[GET /api/cliente]
[HttpGet("{dni}")] ----> string

https://localhost:44344/api/cliente/35095481

---------------------------------------------------------------------------------
8. Agregar busqueda de productos por código.

//[GET /api/producto]
[HttpGet("{codigo}")] ---->string

https://localhost:44344/api/producto/QAZ-123