﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Application.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ApiRestFull.Domain.Models;
using System.Net;
using ApiRestFull.Domain.DTOs;
using Newtonsoft.Json.Linq;

namespace ApiRestFull.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : ControllerBase
    {
        private readonly IServiceProducto _service;

        public ProductoController(IServiceProducto service)
        {
            this._service = service;
        }

        //[POST /api/producto]
        [HttpPost]
        public IActionResult Post([FromBody] ProductoDto producto)
        {
            try
            {
                ProductoDto aproducto = _service.SetProducto(producto);
                return new JsonResult(aproducto) { StatusCode = 201 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

        }

        //[GET /api/producto/GetProductos]
        [HttpGet("GetProductos")]
        public IActionResult GetProductos()
        {
            try
            {
                var productos = _service.GetProductos();
                return new JsonResult(productos) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

        }


        //[GET /api/producto]
        [HttpGet("{codigo}")]
        public IActionResult GetProductobyCodigo(string codigo)
        {
            try
            {
                var producto = _service.GetProductobyCodigo(codigo);
                return new JsonResult(producto) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
    }
}