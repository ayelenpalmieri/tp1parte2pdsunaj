﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ApiRestFull.Application.Services;
using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestFull.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IServiceCliente _service;

        public ClienteController(IServiceCliente service)
        {
            this._service = service;
        }

        //[POST /api/cliente]
        [HttpPost]
        public IActionResult Post([FromBody] ClienteDto cliente)
        {
            try
            {
                ClienteDto acliente = _service.SetCliente(cliente);
                return new JsonResult(acliente) { StatusCode = 201 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

        }

        //[GET /api/cliente]
        [HttpGet]
        public IActionResult GetClientes() 
        {
            try
            {
                var clientes = _service.GetClientes();
                return new JsonResult(clientes) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
        }


        //[GET /api/cliente]
        [HttpGet("{dni}")]
        public IActionResult GetClientebyDNI(string dni)
        {
            try
            {
                var cliente = _service.GetClientebyDNI(dni);
                return new JsonResult(cliente) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
        }

    }
}