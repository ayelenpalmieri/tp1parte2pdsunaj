﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using ApiRestFull.Application.Services;
using ApiRestFull.Domain.DTOs;
using ApiRestFull.Domain.Entities;
using ApiRestFull.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiRestFull.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly IServiceCarrito _service;
        private readonly IServiceVenta _serviceVenta;

        public VentasController(IServiceCarrito service, IServiceVenta serviceVenta)
        {
            this._service = service;
            this._serviceVenta = serviceVenta;
        }


        //[GET /api/ventas]
        [HttpGet("{fecha}")]
        public IActionResult GetListaVentasporFecha(string fecha)
        {
            try
            {
                var listaVentas = _serviceVenta.GetVentas(fecha);
                return new JsonResult(listaVentas) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

        }


        //[GET /api/ventas]
        [HttpGet("fecha/{fecha}/codigo/{codigo}")]
        public IActionResult GetProductoInListado(string Codigo, string fecha)
        {
            try
            {
                var productoinLista = _serviceVenta.GetProductosinLista(Codigo,fecha);
                return new JsonResult(productoinLista) { StatusCode = 200 };
            }
            catch (Exception e)
            {
                return StatusCode((int)HttpStatusCode.BadRequest);
            }

        }


        //[POST /api/ventas]
        [HttpPost]
        public IActionResult PostVenta([FromBody] RequestVentasDto productosPorcliente)
        {
            try
            {
                Cliente cliente = productosPorcliente.cliente;
                List<Producto> productos = productosPorcliente.productos;
                CarritoDto carrito = _service.SetCarrito(cliente);
                foreach (Producto producto in productos)
                {
                    if (producto != null)
                    {
                        _service.SetCarritoProducto(carrito, producto);
                    }
                }

                _serviceVenta.SetVenta(carrito);
                return new JsonResult(productosPorcliente) { StatusCode = 201 };
            }
            catch (Exception e)
            {
                throw e;
                return StatusCode((int)HttpStatusCode.BadRequest);
            }
        }
    }
}