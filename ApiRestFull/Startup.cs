using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using ApiRestFull.AccessData;
using Microsoft.AspNetCore.Mvc;
using ApiRestFull.Domain.Commands;
using ApiRestFull.AccessData.Commands;
using ApiRestFull.Application.Services;
using SqlKata.Compilers;
using System.Data;
using Microsoft.Data.SqlClient;
using ApiRestFull.Domain.Queries;
using ApiRestFull.AccessData.Queries;

namespace ApiRestFull
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //services.AddMvc().AddNewtonsoftJson(
            //  options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            //  );
            services.AddDbContext<ApiRestFullDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ApiRestFullDb")));
            services.AddScoped<ApiRestFullDbContext, ApiRestFullDbContext>();

            services.AddTransient<IGenericsRepository, GenericsRepository>();
            services.AddTransient<IServiceCliente, ServiceCliente>();
            services.AddTransient<IServiceProducto, ServiceProducto>();
            services.AddTransient<IServiceCarrito, ServiceCarrito>();
            services.AddTransient<IServiceVenta, ServiceVenta>();
            services.AddTransient<ICarritoQuery, CarritoQuery>();
            services.AddTransient<IClienteQuery, ClienteQuery>();
            services.AddTransient<IProductoQuery, ProductoQuery>();
            services.AddTransient<IVentasQuery, VentasQuery>();

            services.AddTransient<Compiler, SqlServerCompiler>();
            services.AddTransient<IDbConnection>(b =>
            {
                return new SqlConnection(Configuration.GetConnectionString("ApiRestFullDb"));
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddControllers();
            services.AddControllersWithViews();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                );
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
